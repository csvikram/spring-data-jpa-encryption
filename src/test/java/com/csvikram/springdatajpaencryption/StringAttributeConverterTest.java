package com.csvikram.springdatajpaencryption;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StringAttributeConverterTest {

	private static final Logger log = LoggerFactory.getLogger(StringAttributeConverterTest.class);

	@Autowired
	private CardDetailRepository cardDetailRepository;

	@Test
	void testDataReadDecrypted() {
		CardDetail cardDetail = new CardDetail(null, "Being Crazy Dev", "1234567812345678", "12/30");
		cardDetailRepository.save(cardDetail);

		CardDetail cd = cardDetailRepository.findById(cardDetail.getId()).orElse(new CardDetail());
		Assertions.assertEquals(cardDetail.getCardNumber(), cd.getCardNumber());
		log.info("Name: {}, cardNumber: {}, expiry: {}", cd.getName(), cd.getCardNumber(), cd.getExpiry());
	}

	@Test
	public void testEncryptDataStore() throws SQLException {
		CardDetail cardDetail = new CardDetail(null, "Being Crazy Dev", "1234567812345678", "12/30");
		cardDetailRepository.save(cardDetail);

		Connection con = DriverManager.getConnection("jdbc:h2:mem:db", "sa", "");

		PreparedStatement stmt = con.prepareStatement("select * from card_detail where id = ?");
		stmt.setLong(1, cardDetail.getId());

		ResultSet rs = stmt.executeQuery();
		rs.next();
		String cardNumber = rs.getString("card_number");
		Assertions.assertNotEquals(cardDetail.getCardNumber(), cardNumber);
		log.info("CardNumber in database: {}", cardNumber);

	}

	//To test the effect first update the converter to StringAttributeConverterFirst and when test fail, User the
	// StringAttributeConverter
	@Test
	public void testConcurrentAccess() throws InterruptedException, ExecutionException {
		var cardDetail = new CardDetail(null, "Being Crazy Dev", "1234567812345678", "12/30");
		cardDetailRepository.save(cardDetail);

		ExecutorService executorService = Executors.newFixedThreadPool(16);
		var completableFuture = new ArrayList<CompletableFuture<String>>();
		for (int i = 0; i < 5000; i++) {
			int finalI = i;
			completableFuture.add(CompletableFuture.supplyAsync(() -> {
				cardDetailRepository.save(
						new CardDetail(null, "Being Crazy Dev", "1234567812345678" + finalI, "12/30"));
				return "done";
			}, executorService));
		}

		completableFuture = new ArrayList<>();
		for (int i = 0; i < 5000; i++) {
			int finalI = i;
			completableFuture.add(CompletableFuture.supplyAsync(() -> {
				log.info(cardDetailRepository.findById((long) finalI).orElseGet(CardDetail::new).getCardNumber());
				return "done";
			}, executorService));
		}
		CompletableFuture.allOf(completableFuture.toArray(new CompletableFuture[0])).get();
	}

}