package com.csvikram.springdatajpaencryption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataJpaDatabaseColumnEncryptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataJpaDatabaseColumnEncryptionApplication.class, args);
	}

}
